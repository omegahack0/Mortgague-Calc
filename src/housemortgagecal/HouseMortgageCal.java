        //Author:Matthew Porter McDowell
        //Program Name:HouseMortgageCal
        //Date:9/10/16
        //Class:Programming 1
package housemortgagecal;

import java.text.NumberFormat;
import java.util.Scanner;


public class HouseMortgageCal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        NumberFormat num = NumberFormat.getCurrencyInstance();

        double loanAmount;
        double loanRate;
        double monthlyRate ;
        double yearAmount;
        double amountMonths;
        double monthlyPay;
        String doOver = "y" ;

        while( doOver.equals ("y")){

            //try/catch to get invalid numeric data
            try{

                //Getting numeric value for variables
                System.out.print("Enter loan amount: ");
                loanAmount  = Double.parseDouble(in.nextLine());

                System.out.print("Enter annual rate: ");
                loanRate = Double.parseDouble(in.nextLine());

                System.out.print("Enter the amount of years: ");
                yearAmount = Double.parseDouble(in.nextLine());


                //If statement to verify the user put in a positive number
                if(loanAmount > 0 && loanRate > 0 && yearAmount > 0){

                    //Converting to the correct units
                    //convert years to amount of months
                    amountMonths= yearAmount*12;

                    //Convert loan rate to a percent
                    loanRate= loanRate/100;

                    //converting yearly percent to monthly percent
                    monthlyRate= loanRate/12;

                    //Plugging variables into the formula
                    monthlyPay = monthlyRate * loanAmount *
                            ( Math.pow(monthlyRate + 1 ,amountMonths) /
                                    (Math.pow(monthlyRate + 1 ,amountMonths) - 1));

                    System.out.print("Your monthly payment is: ");
                    System.out.println(num.format(monthlyPay));
                }
                else
                {
                    //statement to make it not an infinate loop
                    System.out.println("Enter a postive number.");
                }
            }
            //catch with the output
            catch  (NumberFormatException e)
            {System.out.println("Enter an Integer!");}
            System.out.println
                    ("Would you like to calculate the again? (y/n):");
            doOver = in.nextLine();

        }
        System.out.println("Thank you come again");

    }

}
